# LocoMeo

## Description
LocoMeo is an Android application designed by Aymeric Robitaille (AyRobi). It aims to record positions of a user at continuous and regular intervals of time in order to determine user habits. Thereby, this application allows a user to know its current location and its most probable future positions. Those predictions are made over the last 24h records. For commodity each user location is automatically associated to a cluster of locations. The later has a unique label which is automatically generated and dynamic.

## Technologies
This project uses the following technologies:
* Kotlin
* Android X API
* Google APIs: for location services
* K-means++: used for location clustering
* Markov model: for location prediction
* Elbow method: used to find a good k in the k-means method

## Author
* ROBITAILLE Aymeric (AyRobi)