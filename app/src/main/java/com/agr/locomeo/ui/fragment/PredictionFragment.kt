package com.agr.locomeo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.agr.locomeo.databinding.FragmentPredictionsBinding
import com.agr.locomeo.model.MarkovMatrix
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.ui.adapter.MarkovAdapter
import com.agr.locomeo.viewmodel.PredictionsViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class PredictionFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PredictionsViewModel

    private var _binding: FragmentPredictionsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(PredictionsViewModel::class.java)

        viewModel.markovMatrix.observe(viewLifecycleOwner, Observer { onMarkovMatrix(it) })

        _binding = FragmentPredictionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewManager = LinearLayoutManager(context)
        val viewAdapter = MarkovAdapter()

        binding.recyclerView.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getMarkovMatrix()
    }

    private fun onMarkovMatrix(result: Resource<MarkovMatrix>) {
        when (result.state) {
            Resource.State.ERROR -> result.message?.let { showToast(result.message) }
            Resource.State.SUCCESS ->
                result.data?.let { markovMatrix ->
                    binding.recyclerView.adapter.apply {
                        if (this is MarkovAdapter)
                            setMarkovMatrix(markovMatrix)
                    }
                }
        }
    }

    private fun showToast(@StringRes resId: Int) {
        activity?.let {
            Toast
                .makeText(activity, resId, Toast.LENGTH_LONG)
                .show()
        }
    }
}