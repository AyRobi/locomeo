package com.agr.locomeo.ui.view

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.text.format.DateFormat
import android.util.AttributeSet
import android.view.LayoutInflater
import com.agr.locomeo.R
import com.agr.locomeo.databinding.BubbleLocationBinding
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.Position
import com.agr.locomeo.model.entity.TaggedPosition
import com.agr.locomeo.util.BitmapUtils
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class CustomMapView : MapView, OnMapReadyCallback, MapboxMap.OnMapClickListener {
    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
    constructor(ctx: Context, attrs: AttributeSet, defStyleAttr: Int) : super(ctx, attrs, defStyleAttr)

    private var mapboxMap: MapboxMap? = null

    private var currentPosSource: GeoJsonSource? = null
    private var currentPosFeatureCollection: FeatureCollection? = null

    private var oldPosSource: GeoJsonSource? = null
    private var oldPosFeatureCollection: FeatureCollection? = null

    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getMapAsync(this)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap

        mapboxMap.setStyle(Style.OUTDOORS) {style ->
            setDefaultCameraPos()
            mapboxMap.addOnMapClickListener(this)

            style.addImage(
                CURRENT_POS_MARKER,
                BitmapUtils.drawableToBitmap(resources.getDrawable(R.drawable.ic_current_position, null))
            )

            style.addImage(
                OLD_POS_MARKER,
                BitmapUtils.drawableToBitmap(resources.getDrawable(R.drawable.ic_old_position, null))
            )

            style.addLayer(
                SymbolLayer(OLD_POS_MARKER_LAYER_ID, OLD_POS_GEOJSON_SOURCE_ID)
                    .withProperties(
                        iconImage(OLD_POS_MARKER),
                        iconAllowOverlap(true),
                        iconOffset(arrayOf(0f, -1f))
                    )
            )

            style.addLayer(
                SymbolLayer(CURRENT_POS_MARKER_LAYER_ID, CURRENT_POS_GEOJSON_SOURCE_ID)
                    .withProperties(
                        iconImage(CURRENT_POS_MARKER),
                        iconAllowOverlap(true),
                        iconOffset(arrayOf(0f, -1f))
                    )
            )

            style.addLayer(
                SymbolLayer(OLD_POS_CALL_OUT_LAYER_ID, OLD_POS_GEOJSON_SOURCE_ID)
                    .withProperties(
                        iconImage("{id}"),
                        iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                        iconAllowOverlap(true),
                        iconOffset(arrayOf(-2f, -2f))
                    )
                    .withFilter(Expression.eq(
                        Expression.get(PROPERTY_SELECTED), Expression.literal(true)
                    ))
            )

            style.addLayer(
                SymbolLayer(CURRENT_POS_CALL_OUT_LAYER_ID, CURRENT_POS_GEOJSON_SOURCE_ID)
                    .withProperties(
                        iconImage(CURRENT_POS_ID),
                        iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                        iconAllowOverlap(true),
                        iconOffset(arrayOf(-2f, -2f))
                    )
                    .withFilter(Expression.eq(
                        Expression.get(PROPERTY_SELECTED), Expression.literal(true)
                    ))
            )

            setupCurrentPosSource()
            setupOldPosSource()
        }
    }

    override fun onMapClick(point: LatLng): Boolean {
        mapboxMap?.let {
            val screenPoint = it.projection.toScreenLocation(point)
            val currentPosFeatures = it.queryRenderedFeatures(screenPoint, CURRENT_POS_MARKER_LAYER_ID)
            val oldPosFeatures = it.queryRenderedFeatures(screenPoint, OLD_POS_MARKER_LAYER_ID)

            currentPosFeatureCollection?.features()?.let { features ->
                if (features.size > 0 && currentPosFeatures.size > 0) {
                    val feature = features[0]
                    feature.addBooleanProperty(
                        PROPERTY_SELECTED,
                        !feature.getBooleanProperty(PROPERTY_SELECTED)
                    )

                    currentPosSource?.setGeoJson(currentPosFeatureCollection)

                    return true
                }
            }

            val oldPosFeatureList = oldPosFeatureCollection?.features() ?: return false

            if (oldPosFeatures.isEmpty()) {
                for (feature in oldPosFeatureList)
                    feature.addBooleanProperty(PROPERTY_SELECTED, false)

                oldPosSource?.setGeoJson(oldPosFeatureCollection)
                return true
            }

            val id = oldPosFeatures[0].getStringProperty(PROPERTY_ID)
            for (feature in oldPosFeatureList) {
                if (feature.getStringProperty(PROPERTY_ID) == id) {
                    feature.addBooleanProperty(
                        PROPERTY_SELECTED,
                        !feature.getBooleanProperty(PROPERTY_SELECTED)
                    )
                } else {
                    feature.addBooleanProperty(PROPERTY_SELECTED, false)
                }
            }

            oldPosSource?.setGeoJson(oldPosFeatureCollection)

            return true
        }

        return false
    }

    private fun setDefaultCameraPos() {
        if (currentPosFeatureCollection == null)
            zoomTo(LatLng(47.63, 6.86)) //BELFORT
        else
            zoomToCurrentPosition()
    }

    private fun setupOldPosSource() {
        if (oldPosFeatureCollection == null)
            return

        val features = oldPosFeatureCollection?.features() ?: return
        coroutineScope.launch { createBubbleImage(features) }

        oldPosSource?.setGeoJson(oldPosFeatureCollection)

        if (oldPosSource == null)
            mapboxMap?.let {
                it.getStyle {style ->
                    val source = GeoJsonSource(OLD_POS_GEOJSON_SOURCE_ID, oldPosFeatureCollection)
                    if (oldPosSource == null) {
                        oldPosSource = source
                        style.addSource(source)
                    }
                }
            }

    }

    private fun setupCurrentPosSource() {
        mapboxMap?.let { mapbox ->
            currentPosFeatureCollection?.let { featureCollection ->
                if (currentPosSource == null) {
                    val source = GeoJsonSource(CURRENT_POS_GEOJSON_SOURCE_ID, featureCollection)
                    currentPosSource = source
                    mapbox.getStyle { style ->
                        style.addSource(source)
                    }
                }

                currentPosSource?.setGeoJson(featureCollection)

                val features = featureCollection.features() ?: return
                coroutineScope.launch { createBubbleImage(features) }
            }
        }
    }

    private fun setBubbleImage(imageMap: HashMap<String, Bitmap>) {
        mapboxMap?.let {
            it.getStyle {style ->
                style.addImages(imageMap)
            }
        }
    }

    private suspend fun createBubbleImage(features: Collection<Feature>) {
        withContext(Dispatchers.Default) {
            val imageMap = HashMap<String, Bitmap>()
            val inflater = LayoutInflater.from(context)

            for (feature in features) {
                val binding  = BubbleLocationBinding.inflate(inflater)

                val id = feature.getStringProperty(PROPERTY_ID)

                binding.apply {
                    tag.text = feature.getStringProperty(PROPERTY_TAG)
                    count.text = feature.getNumberProperty(PROPERTY_COUNT).toString()
                    latitute.text = feature.getNumberProperty(PROPERTY_LAT).toString()
                    longitude.text = feature.getNumberProperty(PROPERTY_LONG).toString()
                    time.text = DateFormat.format(
                        "yyyy-MM-dd hh:mm:ss",
                        Date(feature.getNumberProperty(PROPERTY_TIME).toLong())
                    )
                    bearing.text = feature.getNumberProperty(PROPERTY_BEARING).formatFloat()
                    accuracy.text = feature.getNumberProperty(PROPERTY_ACCURACY).formatFloat()
                    altitude.text = feature.getNumberProperty(PROPERTY_ALTITUDE).formatFloat()
                }

                imageMap[id] = BitmapUtils.generateBitmap(binding.root)
            }

            withContext(Dispatchers.Main) {
                setBubbleImage(imageMap)
            }
        }
    }

    private fun clearOldPos() {
        oldPosFeatureCollection?.features()?.let { features ->
            mapboxMap?.getStyle {style ->
                for (feature in features)
                    style.removeImage(feature.getStringProperty(PROPERTY_ID))
            }
        }

        oldPosFeatureCollection = null
    }

    private fun zoomTo(latLng: LatLng) {
        mapboxMap?.cameraPosition = CameraPosition.Builder()
            .zoom(ZOOM_LEVEL)
            .target(latLng)
            .build()
    }

    fun zoomToCurrentPosition() {
        currentPosFeatureCollection?.features()?.firstOrNull()?.let {
            zoomTo(LatLng(
                it.getNumberProperty(PROPERTY_LAT).toDouble(),
                it.getNumberProperty(PROPERTY_LONG).toDouble()
            ))
        }
    }

    fun zoomToPosition(position: Position) {
        zoomTo(LatLng(position.latitude, position.longitude))
    }

    fun setLastPosition(position: TaggedPosition) {
        val selected = currentPosFeatureCollection?.features()?.firstOrNull()?.getBooleanProperty(
            PROPERTY_SELECTED) ?: false

        currentPosFeatureCollection = FeatureCollection.fromFeature(
            createFeatureFromPos(
                GlobalizedPosition(
                    CURRENT_POS_ID,
                    position.tag,
                    1,
                    position.position
                ),
                selected = selected
            )
        )

        setupCurrentPosSource()
    }

    fun setClusters(clusters : List<Cluster>) {
        val features = ArrayList<Feature>()

        for (cluster in clusters)
            features.add(createFeatureFromPos(
                GlobalizedPosition(
                    cluster.label,
                    cluster.label,
                    cluster.count,
                    cluster.centroid
                )
            ))

        clearOldPos()
        oldPosFeatureCollection = FeatureCollection.fromFeatures(features)
        setupOldPosSource()
    }

    private data class GlobalizedPosition(
        val id: String,
        val tag: String,
        val count: Int,
        val position: Position
        )

    companion object {
        private const val ZOOM_LEVEL = 18.0

        private const val CURRENT_POS_GEOJSON_SOURCE_ID = "CURRENT_POS_GEOJSON_SOURCE_ID"
        private const val OLD_POS_GEOJSON_SOURCE_ID = "OLD_POS_GEOJSON_SOURCE_ID"
        private const val CURRENT_POS_MARKER = "CURRENT_POS_MARKER"
        private const val CURRENT_POS_MARKER_LAYER_ID = "CURRENT_POS_MARKER_LAYER_ID"
        private const val OLD_POS_MARKER = "OLD_POS_MARKER"
        private const val OLD_POS_MARKER_LAYER_ID = "OLD_POS_MARKER_LAYER_ID"
        private const val OLD_POS_CALL_OUT_LAYER_ID = "OLD_POS_CALL_OUT_LAYER_ID"
        private const val CURRENT_POS_CALL_OUT_LAYER_ID = "CURRENT_POS_CALL_OUT_LAYER_ID"
        private const val CURRENT_POS_ID = "CURRENT_POS_CALL_OUT_MARKER"
        private const val PROPERTY_SELECTED = "selected"
        private const val PROPERTY_ID = "id"
        private const val PROPERTY_TAG = "position_tag"
        private const val PROPERTY_COUNT = "count"
        private const val PROPERTY_TIME = "time"
        private const val PROPERTY_BEARING = "bearing"
        private const val PROPERTY_ACCURACY = "accuracy"
        private const val PROPERTY_LAT = "latitude"
        private const val PROPERTY_LONG = "longitude"
        private const val PROPERTY_ALTITUDE = "altitude"

        private fun Number.formatFloat() = String.format("%.3f", this)

        private fun createFeatureFromPos(globalizedPosition: GlobalizedPosition, selected: Boolean = false): Feature {
            val feature =
                Feature.fromGeometry(Point.fromLngLat(
                    globalizedPosition.position.longitude, globalizedPosition.position.latitude
                ))

            globalizedPosition.position.apply {
                feature.addNumberProperty(PROPERTY_TIME, time)
                feature.addNumberProperty(PROPERTY_LAT, latitude)
                feature.addNumberProperty(PROPERTY_LONG, longitude)
                feature.addNumberProperty(PROPERTY_ACCURACY, accuracy)
                feature.addNumberProperty(PROPERTY_ALTITUDE, altitude)
                feature.addNumberProperty(PROPERTY_BEARING, bearing)
            }

            feature.addBooleanProperty(PROPERTY_SELECTED, selected)
            feature.addStringProperty(PROPERTY_ID, globalizedPosition.id)
            feature.addStringProperty(PROPERTY_TAG, globalizedPosition.tag)
            feature.addNumberProperty(PROPERTY_COUNT, globalizedPosition.count)

            return feature
        }
    }
}