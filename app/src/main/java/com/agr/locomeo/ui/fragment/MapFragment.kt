package com.agr.locomeo.ui.fragment

import android.Manifest
import android.app.Activity
import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.agr.locomeo.databinding.FragmentMapBinding
import com.agr.locomeo.model.LocationServiceRunner
import com.agr.locomeo.model.Params.Companion.ACTION_BROADCAST_NEW_SETTINGS
import com.agr.locomeo.model.Params.Companion.EXTRA_IS_LOCATION_ALLOWED
import com.agr.locomeo.model.Params.Companion.LOCATION_AVAILABILITY_BROADCAST
import com.agr.locomeo.model.Updatable
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.model.entity.TaggedPosition
import com.agr.locomeo.util.LocationUtils.Companion.isPermissionGranted
import com.agr.locomeo.viewmodel.MapViewModel
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class MapFragment : DaggerFragment(), Updatable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mapViewModel: MapViewModel
    private lateinit var requestPermissions: ActivityResultLauncher<Array<out String>>
    private lateinit var requestLocation: ActivityResultLauncher<IntentSenderRequest>
    private lateinit var receiver: MyReceiver

    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!! // valid only between onCreateView and onDestroyView

    private var isFirstPosition = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isFirstPosition = true
        receiver = MyReceiver()

        mapViewModel = ViewModelProvider(this, viewModelFactory)
            .get(MapViewModel::class.java)

        mapViewModel.lastPosition.observe(viewLifecycleOwner, Observer { onLastPosition(it) })
        mapViewModel.kPositions.observe(viewLifecycleOwner, Observer { onKPositions(it) })

        _binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mapView.onCreate(savedInstanceState)

        binding.locateButton.setOnClickListener { askLocationAccess() }
        binding.goToCurrentPosButton.setOnClickListener {binding.mapView.zoomToCurrentPosition()}

        updateLocateState(false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        requestPermissions = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) {
            updateLocateState()
        }

        requestLocation = registerForActivityResult(
            ActivityResultContracts.StartIntentSenderForResult()
        ) {result ->
            if (result.resultCode == Activity.RESULT_OK)
                updateLocateState(false)
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()

        mapViewModel.startLastPositionObservation()
        mapViewModel.startKPositionsObservation()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()

        activity?.let {activity ->
            IntentFilter().apply {
                addAction(LOCATION_AVAILABILITY_BROADCAST)
                addAction(ACTION_BROADCAST_NEW_SETTINGS)
            }.also {
                LocalBroadcastManager.getInstance(activity).registerReceiver(
                    receiver,
                    it
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()

        activity?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(receiver)
        }
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
        mapViewModel.stopLastPositionObservation()
        mapViewModel.stopKPositionsObservation()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        _binding?.let {
            binding.mapView.onSaveInstanceState(outState)
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.mapView.onDestroy()

        _binding = null
    }

    private fun onLastPosition(position: Resource<TaggedPosition>) {
        if (position.state == Resource.State.SUCCESS) {
            position.data?.let {
                binding.mapView.setLastPosition(it)

                if (isFirstPosition) {
                    binding.mapView.zoomToPosition(it.position)
                    isFirstPosition = false
                }
            }
        }
    }

    private fun onKPositions(positions: Resource<List<Cluster>>) {
        when (positions.state) {
            Resource.State.SUCCESS -> positions.data?.let {
                binding.mapView.setClusters(it)
            }

            Resource.State.ERROR -> positions.message?.let {
                Toast.makeText(context, it, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun askLocationAccess() {
        requestPermissions.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
    }

    private fun updateLocateState(showPopUp: Boolean = true) {
        val granted = isPermissionGranted(context)

        if (!granted) {
            showLocateButton(true)
            return
        }

        context?.let {ctx ->
            val locationRequest = mapViewModel.getLocationRequest()

            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            val client = LocationServices.getSettingsClient(ctx)
            val task = client.checkLocationSettings(builder.build())
            task.addOnSuccessListener {
                showLocateButton(!granted)

                if (granted && activity is LocationServiceRunner)
                    (activity as LocationServiceRunner).startLocationService()
            }
            task.addOnFailureListener {
                showLocateButton(true)

                if (!showPopUp)
                    return@addOnFailureListener

                if (it is ResolvableApiException) {
                    try {
                        val reqBuilder = IntentSenderRequest.Builder(it.resolution.intentSender)
                        requestLocation.launch(reqBuilder.build())
                    } catch (e : IntentSender.SendIntentException) {
                        // Empty
                    }
                }
            }
        }
    }

    private fun showLocateButton(show: Boolean = true) {
        view?.let {_ ->
            binding.locateButton.visibility = if (!show) View.GONE else View.VISIBLE
        }
    }

    inner class MyReceiver: BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action) {
                ACTION_BROADCAST_NEW_SETTINGS -> onSettingsUpdate()
                LOCATION_AVAILABILITY_BROADCAST -> this@MapFragment.showLocateButton(
                    !intent.getBooleanExtra(EXTRA_IS_LOCATION_ALLOWED, false)
                )
            }
        }

    }

    override fun onSettingsUpdate() {
        mapViewModel.updateInternalData()
    }
}