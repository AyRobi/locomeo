package com.agr.locomeo.ui.adapter

import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.agr.locomeo.databinding.ViewPositionBinding
import com.agr.locomeo.model.entity.Position
import java.util.*

class PositionAdapter : RecyclerView.Adapter<PositionAdapter.PositionViewHolder>() {
    class PositionViewHolder(private val binding: ViewPositionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Position) {
            binding.apply {
                accuracy.text = position.accuracy.toString()
                altitude.text = position.altitude.toString()
                bearing.text = position.bearing.toString()
                time.text = DateFormat.format(
                    "yyyy-MM-dd hh:mm:ss",
                    Date(position.time)
                )
                latitute.text = position.latitude.toString()
                longitude.text = position.longitude.toString()
            }
        }
    }

    private val positionList: SortedList<Position> = SortedList(
        Position::class.java, object : SortedListAdapterCallback<Position>(this) {
        override fun compare(o1: Position, o2: Position): Int {
            return (o2.time - o1.time).toInt()
        }

        override fun areContentsTheSame(oldItem: Position, newItem: Position): Boolean {
            return oldItem.time == newItem.time
        }

        override fun areItemsTheSame(item1: Position, item2: Position): Boolean {
            return item1 == item2
        }
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PositionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewPositionBinding.inflate(inflater, parent, false)
        return PositionViewHolder(binding)
    }


    override fun getItemCount() = positionList.size()

    override fun onBindViewHolder(holder: PositionViewHolder, position: Int) {
        holder.bind(positionList[position])
    }

    fun addPositions(positions: List<Position>) {
        positionList.addAll(positions)
    }

}