package com.agr.locomeo.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.agr.locomeo.databinding.FragmentSettingsBinding
import com.agr.locomeo.model.KMeansPP
import com.agr.locomeo.model.Params.Companion.ACTION_BROADCAST_NEW_SETTINGS
import com.agr.locomeo.model.Updatable
import com.agr.locomeo.util.PositiveFloatFilter
import com.agr.locomeo.util.PositiveIntFilter
import com.agr.locomeo.viewmodel.SettingsViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SettingsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SettingsViewModel

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(SettingsViewModel::class.java)
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val settings = viewModel.getSettings()

        binding.apply {
            maxSaveAccuracyInput.filters += PositiveFloatFilter()
            maxSaveAccuracyInput.setText(settings.maxSaveAccuracy.formatNumber())

            maxComputeAccuracyInput.filters += PositiveFloatFilter()
            maxComputeAccuracyInput.setText(settings.maxComputeAccuracy.formatNumber())

            maxLocationRequestIntervalInput.filters += PositiveFloatFilter()
            maxLocationRequestIntervalInput.setText(
                (settings.locationRequestInterval / S_SCALE).formatNumber()
            )

            maxLocationRequestFastestIntervalInput.filters += PositiveFloatFilter()
            maxLocationRequestFastestIntervalInput.setText(
                (settings.locationRequestFastestInterval / S_SCALE).formatNumber()
            )

            predictionDurationInput.filters += PositiveFloatFilter()
            predictionDurationInput.setText(
                (settings.predictionDuration / H_SCALE).formatNumber()
            )

            clusteringNumberInput.filters += PositiveIntFilter()
            clusteringNumberInput.setText(settings.clusteringNumber.toString())

            clusteringMethodSpinner.adapter = ArrayAdapter(
                view.context,
                android.R.layout.simple_spinner_dropdown_item,
                KMeansPP.ClusteringMethod.values()
            )
            clusteringMethodSpinner.setSelection(settings.clusteringMethod.ordinal)

            clearDatabaseButton.setOnClickListener { viewModel.clearDataBase() }
            saveButton.setOnClickListener { save() }
        }
    }

    private fun save() {
        binding.apply {
            val oldSettings = viewModel.getSettings()
            val newSettings = SettingsViewModel.Settings(
                maxSaveAccuracyInput.toPositiveFloatOrNull()
                    ?: oldSettings.maxSaveAccuracy,
                maxComputeAccuracyInput.toPositiveFloatOrNull()
                    ?: oldSettings.maxComputeAccuracy,
                maxLocationRequestIntervalInput.toPositiveFloatOrNull()?.times(S_SCALE)?.toLong()
                    ?: oldSettings.locationRequestInterval,
                maxLocationRequestFastestIntervalInput.toPositiveFloatOrNull()?.times(S_SCALE)?.toLong()
                    ?: oldSettings.locationRequestFastestInterval,
                KMeansPP.ClusteringMethod.valueOf(clusteringMethodSpinner.selectedItem.toString()),
                clusteringNumberInput.toPositiveIntOrNull()
                    ?: oldSettings.clusteringNumber,
                predictionDurationInput.toPositiveFloatOrNull()?.times(H_SCALE)?.toLong()
                    ?: oldSettings.predictionDuration
            )

            viewModel.setSettings(newSettings)
        }

        context?.let {
            val intent = Intent(ACTION_BROADCAST_NEW_SETTINGS)
            LocalBroadcastManager.getInstance(it)
                .sendBroadcast(intent)
        }

        activity?.let {
            if (it is Updatable)
                it.onSettingsUpdate()
        }
    }

    companion object {
        private const val S_SCALE = 1000f
        private const val H_SCALE = 60 * 60 * 1000f
        private fun Float.formatNumber() = String.format("%.3f", this)
        private fun EditText.toPositiveFloatOrNull() = PositiveFloatFilter
            .toPositiveFloatOrNull(text.toString())
        private fun EditText.toPositiveIntOrNull() = PositiveIntFilter
            .toPositiveIntOrNull(text.toString())
    }
}