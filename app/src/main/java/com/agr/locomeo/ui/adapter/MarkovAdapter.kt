package com.agr.locomeo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.agr.locomeo.R
import com.agr.locomeo.databinding.ViewMarkovBinding
import com.agr.locomeo.model.MarkovMatrix

class MarkovAdapter : RecyclerView.Adapter<MarkovAdapter.MarkovViewHolder>() {
    class MarkovViewHolder(private val binding: ViewMarkovBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(rowLabel: String, matrix: MarkovMatrix) {
            binding.apply {
                fromLabel.text = root.context.getString(R.string.view_markov_from, rowLabel)
                toLayout.removeAllViews()
                matrix.getToProbabilities(rowLabel)?.let {
                    for (entry in it)
                        toLayout.addView(TextView(root.context).apply {
                            text = binding.root.context.getString(
                                R.string.view_markov_to, entry.key, entry.value * 100
                            )
                            layoutParams = LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )
                        })
                }
            }
        }
    }

    private var markovRows: List<String> = ArrayList()
    private var markovMatrix: MarkovMatrix? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkovViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewMarkovBinding.inflate(inflater, parent, false)
        return MarkovViewHolder(binding)
    }


    override fun getItemCount() = markovRows.size

    override fun onBindViewHolder(holder: MarkovViewHolder, position: Int) {
        markovMatrix?.let {
            holder.bind(
                markovRows[position],
                it
            )
        }
    }

    fun setMarkovMatrix(markovMatrix: MarkovMatrix) {
        this.markovMatrix = markovMatrix
        markovRows = markovMatrix.getRowLabels().toList()

        notifyDataSetChanged()
    }
}