package com.agr.locomeo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.agr.locomeo.databinding.FragmentDebugBinding
import com.agr.locomeo.model.entity.Position
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.ui.adapter.PositionAdapter
import com.agr.locomeo.viewmodel.DebugViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class DebugFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var debugViewModel: DebugViewModel

    private var _binding: FragmentDebugBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        debugViewModel = ViewModelProvider(this, viewModelFactory)
            .get(DebugViewModel::class.java)

        debugViewModel.positions.observe(viewLifecycleOwner, Observer { onPositions(it) })

        _binding = FragmentDebugBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewManager = LinearLayoutManager(context)
        val viewAdapter = PositionAdapter()

        binding.recyclerView.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        debugViewModel.startPositionObservation()
    }

    override fun onStop() {
        super.onStop()
        debugViewModel.stopPositionObservation()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onPositions(positions: Resource<List<Position>>) {
        val adapter = binding.recyclerView.adapter

        if (adapter is PositionAdapter)
            positions.data?.let {
                adapter.addPositions(it)
            }
    }
}