package com.agr.locomeo.viewmodel

import android.content.Context
import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.agr.locomeo.R
import com.agr.locomeo.extension.postSuccess
import com.agr.locomeo.extension.setError
import com.agr.locomeo.extension.setLoading
import com.agr.locomeo.model.entity.Resource
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import javax.inject.Inject

class SplashViewModel @Inject constructor(_ctx: Context) : ViewModel() {
    private val context = _ctx
    private val _isReady = MutableLiveData<Resource<Boolean>>()

    // Exported LiveData
    val isReady: LiveData<Resource<Boolean>> = _isReady

    init {
        _isReady.setLoading()
    }

    fun checkGoogleService() {
        val availability = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)

        if (availability != ConnectionResult.SUCCESS) {
            _isReady.apply { setError(R.string.activity_splash_google_service_error) }
            return
        }

        //Delay a little bit to see the logo
        val handler = Handler()
        handler.postDelayed({ _isReady.apply { postSuccess(true) } }, 666)
    }
}