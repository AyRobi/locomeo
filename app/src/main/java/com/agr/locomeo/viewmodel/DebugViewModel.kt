package com.agr.locomeo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.agr.locomeo.extension.postSuccess
import com.agr.locomeo.extension.setLoading
import com.agr.locomeo.model.entity.Position
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.model.storage.DataBase
import kotlinx.coroutines.*
import javax.inject.Inject

class DebugViewModel @Inject constructor(_db: DataBase) : ViewModel() {
    private val db = _db

    private val _positions = MutableLiveData<Resource<List<Position>>>()
    val positions: LiveData<Resource<List<Position>>> = _positions

    private var locationJob: Job? = null

    fun startPositionObservation() {
        _positions.setLoading()

        stopPositionObservation()
        locationJob = viewModelScope.launch { locationPolling() }
    }

    fun stopPositionObservation() {
        locationJob?.cancel()
    }

    private suspend fun locationPolling() = withContext(Dispatchers.Default) {
        delay(128)

        while (isActive) {

            val lastTime = _positions.value?.data?.firstOrNull()?.time ?: 0L
            val result = db.getLastEntries(lastTime)
            _positions.postSuccess(result)

            delay(FETCH_DELAY)
        }
    }

    companion object {
        private const val FETCH_DELAY = 5_000L
    }
}