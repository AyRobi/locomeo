package com.agr.locomeo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.agr.locomeo.R
import com.agr.locomeo.extension.postError
import com.agr.locomeo.extension.postSuccess
import com.agr.locomeo.extension.setLoading
import com.agr.locomeo.model.MarkovMatrix
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.model.storage.DataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PredictionsViewModel @Inject constructor(
    private val db: DataBase
) : ViewModel() {

    private val _markovMatrix = MutableLiveData<Resource<MarkovMatrix>>()
    val markovMatrix: LiveData<Resource<MarkovMatrix>> = _markovMatrix

    fun getMarkovMatrix() {
        _markovMatrix.setLoading()
        viewModelScope.launch { asyncGetMarkovMatrix() }
    }

    private suspend fun asyncGetMarkovMatrix() = withContext(Dispatchers.Default) {
        val clusteredPosition = db.getClusteredPosition()

        if (clusteredPosition.size < 2)
            _markovMatrix.postError(R.string.fragment_prediction_empty_matrix)
        else
            _markovMatrix.postSuccess(MarkovMatrix.fromClusteredPositions(clusteredPosition))
    }
}