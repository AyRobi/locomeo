package com.agr.locomeo.viewmodel

import androidx.lifecycle.ViewModel
import com.agr.locomeo.model.KMeansPP
import com.agr.locomeo.model.storage.DataBase
import com.agr.locomeo.model.storage.Storage
import javax.inject.Inject
import kotlin.concurrent.thread

class SettingsViewModel @Inject constructor(
    private val storage: Storage,
    private val db: DataBase
) : ViewModel() {
    data class Settings(
        val maxSaveAccuracy: Float,
        val maxComputeAccuracy: Float,
        val locationRequestInterval: Long,
        val locationRequestFastestInterval: Long,
        val clusteringMethod: KMeansPP.ClusteringMethod,
        val clusteringNumber: Int,
        val predictionDuration: Long
    )

    fun getSettings(): Settings {
        return Settings(
            storage.getMaxRegisteringAccuracy(),
            storage.getMaxComputeAccuracy(),
            storage.getLocationRequestInterval(),
            storage.getLocationRequestFastestInterval(),
            storage.getClusteringMethod(),
            storage.getClusterNumber(),
            storage.getPredictionDuration()
        )
    }

    fun setSettings(value: Settings) {
        storage.apply {
            setMaxRegisteringAccuracy(value.maxSaveAccuracy)
            setMaxComputeAccuracy(value.maxComputeAccuracy)
            setLocationRequestInterval(value.locationRequestInterval)
            setLocationRequestFastestInterval(value.locationRequestFastestInterval)
            setClusteringMethod(value.clusteringMethod)
            setClusterNumber(value.clusteringNumber)
            setPredictionDuration(value.predictionDuration)
        }
    }

    fun clearDataBase() {
        thread(start = true) {
            db.clear()
        }
    }

}