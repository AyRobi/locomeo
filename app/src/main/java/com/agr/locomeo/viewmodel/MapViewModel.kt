package com.agr.locomeo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.agr.locomeo.R
import com.agr.locomeo.extension.postError
import com.agr.locomeo.extension.postSuccess
import com.agr.locomeo.extension.setLoading
import com.agr.locomeo.model.KMeansPP
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.Resource
import com.agr.locomeo.model.entity.TaggedPosition
import com.agr.locomeo.model.storage.DataBase
import com.agr.locomeo.model.storage.Storage
import com.agr.locomeo.service.LocationService
import kotlinx.coroutines.*
import javax.inject.Inject

class MapViewModel @Inject constructor(
    private val db: DataBase,
    private val storage: Storage
) : ViewModel() {

    private var isKPolling = false

    private val _lastPosition = MutableLiveData<Resource<TaggedPosition>>()
    val lastPosition: LiveData<Resource<TaggedPosition>> = _lastPosition

    private val _kPositions = MutableLiveData<Resource<List<Cluster>>>()
    val kPositions: LiveData<Resource<List<Cluster>>> = _kPositions

    private var lastPositionJob: Job? = null
    private var kPositionJob: Job? = null

    fun startLastPositionObservation() {
        isKPolling = true
        _lastPosition.setLoading()
        stopLastPositionObservation()
        lastPositionJob = viewModelScope.launch { lastPositionPolling() }
    }

    fun startKPositionsObservation() {
        _kPositions.setLoading()
        stopKPositionsObservation()
        kPositionJob = viewModelScope.launch { kPositionPolling(getParams()) }
    }

    fun stopLastPositionObservation() {
        lastPositionJob?.cancel()
    }

    fun stopKPositionsObservation() {
        kPositionJob?.cancel()
    }

    private fun getParams() = ClusteringParams(
        storage.getMaxComputeAccuracy(),
        storage.getClusteringMethod(),
        storage.getClusterNumber(),
        storage.getPredictionDuration()
    )

    private suspend fun lastPositionPolling() = withContext(Dispatchers.Default) {
        while (isActive) {
            val result = db.getLastPosition()

            result?.let {
                val clusters = db.getClusters()

                _lastPosition.postSuccess(TaggedPosition.fromClusters(
                    clusters,
                    result
                ))
            }

            delay(CURRENT_POSITION_FETCH_DELAY)
        }
    }

    private suspend fun kPositionPolling(clusteringParams: ClusteringParams) = withContext(Dispatchers.Default) {
        val minK = if (clusteringParams.clusterNumber / 2 >= 2) clusteringParams.clusterNumber / 2 else 2
        val maxK = 2 * clusteringParams.clusterNumber

        db.getClusters().let {
            _kPositions.postSuccess(it)
        }

        delay(
            storage.getLastClusteringComputationTime() - System.currentTimeMillis() +
                    K_POSITION_FETCH_DELAY
        )

        while (isActive) {
            val minTime = System.currentTimeMillis() - clusteringParams.predictionDuration
            val results = db.getLastEntries(minTime, clusteringParams.maxAccuracy)
            var sleepTime = K_POSITION_FETCH_DELAY

            val kMeansPP = KMeansPP(results)

            try {
                val kClusters =
                    if (clusteringParams.clusteringMethod == KMeansPP.ClusteringMethod.CONSTANT)
                        kMeansPP.fit(clusteringParams.clusterNumber)
                    else
                        kMeansPP.elbowFit(minK, maxK)

                if (kClusters.isNotEmpty()) {
                    val labels = Cluster.autoCreateLabels(kClusters.size)
                    val clusters = db.updateClusters(labels, kClusters)

                    _kPositions.postSuccess(clusters)
                } else {
                    sleepTime = storage.getLocationRequestFastestInterval()
                }
            } catch (e: ArithmeticException) {
                _kPositions.postError(R.string.fragment_map_convergence_error)
            }

            storage.setLastClusteringComputationTime(System.currentTimeMillis())
            delay(sleepTime)
        }
    }

    fun getLocationRequest() = LocationService.getLocationRequest(storage)

    fun updateInternalData() {
        if (isKPolling)
            startKPositionsObservation()
    }

    private data class ClusteringParams(
        val maxAccuracy: Float,
        val clusteringMethod: KMeansPP.ClusteringMethod,
        val clusterNumber: Int,
        val predictionDuration: Long
    )

    companion object {
        private const val CURRENT_POSITION_FETCH_DELAY = 5_000L // in ms
        private const val K_POSITION_FETCH_DELAY = 15 * 60 * 1000L  // in ms
    }
}