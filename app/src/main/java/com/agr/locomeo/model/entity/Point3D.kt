package com.agr.locomeo.model.entity

import kotlin.math.pow

data class Point3D(
    val x: Double,
    val y: Double,
    val z: Double
) {
    fun squaredEuclideanDistanceTo(p: Point3D) =
        (x - p.x).pow(2) + (y - p.y).pow(2) + (z - p.z).pow(2)
}