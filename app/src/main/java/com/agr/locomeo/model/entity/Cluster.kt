package com.agr.locomeo.model.entity

import androidx.room.*

@Entity(tableName = "clusters")
data class Cluster(
    @ColumnInfo(name = "cluster_id") @PrimaryKey val label: String,

    @Embedded
    val centroid: Position,

    @ColumnInfo(name = "position_count")
    val count: Int
) {

    companion object{
        private val alphabet = arrayOf(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        )

        fun autoCreateLabels(n: Int): List<String> {
            if (n < 0)
                throw IllegalArgumentException("The number of labels must be positive")

            val labels = ArrayList<String>(n)

            for (i in 0 until n) {
                var ind = i
                val builder = StringBuilder()

                do {
                    val j = ind % 26
                    ind /= 26
                    builder.append(alphabet[j])
                } while (ind != 0)

                labels.add(builder.toString())
            }

            return labels
        }
    }
}