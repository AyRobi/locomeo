package com.agr.locomeo.model.entity

data class TaggedPosition(
    val tag: String,
    val position: Position
) {
    companion object{
        fun fromClusters(clusters: List<Cluster>, position: Position): TaggedPosition {
            var minDist = Double.MAX_VALUE
            var minLabel = "?"

            for (cluster in clusters) {
                val d = cluster.centroid.squaredEuclideanDistanceTo(position)

                if (d < minDist) {
                    minDist = d
                    minLabel = cluster.label
                }
            }

            return TaggedPosition(
                minLabel,
                position
            )
        }
    }
}