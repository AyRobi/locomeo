package com.agr.locomeo.model

import com.agr.locomeo.model.entity.Position
import kotlin.math.abs
import kotlin.random.Random

class KMeansPP(
    _positions: List<Position>,
    _maxIterations: Int = DEFAULT_MAX_ITERATION
)  {
    private val positions = _positions
    private val n = _positions.size
    private val maxIterations = _maxIterations

    fun elbowFit(minK: Int = DEFAULT_MIN_K, maxK: Int = DEFAULT_MAX_K): Map<Position, List<Position>> {
        if (maxK < 1 || minK < 1)
            throw IllegalArgumentException("min and max K must be at lest equal to 1")

        var actualFit = fit(minK)
        val n = maxK - minK + 1
        val fits = ArrayList<Map<Position, List<Position>>>(n)
        val inertia = ArrayList<Double>(n + 2)

        // Reserve space for the first interpolated inertia
        inertia.add(0.0)

        fits.add(actualFit)

        // Compute inertia for all possible k
        for (k in (minK + 1)..maxK) {
            if (actualFit.keys.size < k - 1)
                break

            inertia.add(computeInertia(actualFit))

            actualFit = fit(k)
            fits.add(actualFit)
        }

        if (inertia.size > 2) {

            // Interpolate first point
            inertia[0] = interpolateInertiaAt(
                0,
                1, inertia[1],
                2, inertia[2]
            )

            // Interpolate last point
            inertia.add(interpolateInertiaAt(
                inertia.size,
                inertia.lastIndex - 1, inertia[inertia.lastIndex - 1],
                inertia.lastIndex, inertia.last()
            ))

            // Find the elbow point, this will be our best k
            var maxSecondDerivative = Double.NEGATIVE_INFINITY
            for (i in 1 until inertia.size - 1) {
                /*
                 * Compute the absolute second derivative to find the elbow, this one is defined to
                 * be the one with the maximum absolute second derivative
                 */
                val secondDerivative = abs(inertia[i + 1] + inertia[i - 1] - 2 * inertia[i])

                if (secondDerivative > maxSecondDerivative) {
                    maxSecondDerivative = secondDerivative
                    actualFit = fits[i - 1]
                }
            }
        }

        return actualFit
    }

    fun fit(k: Int): Map<Position, List<Position>> {
        if (k <= 0)
            throw IllegalArgumentException("k must be strictly positive")

        val clusters = HashMap<Position, ArrayList<Position>>()

        if (n <= k) {
            for (position in positions)
                clusters[position] = arrayListOf(position)

            return clusters
        }

        val assignments = Array(positions.size) { 0 }

        val centroids = createInitialCentroids(k)
        var newCentroids = centroids

        for (i in 0 until maxIterations) {
            var hasEmpty: Boolean

            val hasChanged = assignPositionToCentroids(newCentroids, assignments, clusters)

            if (!hasChanged)
                break

            newCentroids = ArrayList(k)

            // Compute new centroids with avoiding empty clusters
            do {
                hasEmpty = false
                newCentroids.clear()

                for (cluster in clusters.entries) {
                    if (cluster.value.isEmpty()) {
                        val sol = solveEmptyCluster(clusters)
                        val taken = clusters[sol.takenFromCentroid]
                        taken!!.removeAt(sol.selectionIndexInCluster)
                        clusters.remove(sol.takenFromCentroid)
                        clusters[computeCentroidOf(taken)] = taken

                        clusters.remove(cluster.key)
                        clusters[sol.newCentroid] = arrayListOf(sol.newCentroid)

                        hasEmpty = true
                        break
                    } else {
                        newCentroids.add(
                            computeCentroidOf(cluster.value)
                        )
                    }
                }
            } while (hasEmpty)
        }

        return clusters
    }

    private fun createInitialCentroids(k: Int): ArrayList<Position> {
        val centroids = ArrayList<Position>()
        val taken = Array(positions.size) {false}
        val firstInd = random.nextInt(n)
        var nextCentroid = positions[firstInd]

        // Choose a first random centroid
        centroids.add(nextCentroid)
        taken[firstInd] = true

        val minSquaredDist = Array(positions.size) {0.0}
        for ((i, position) in positions.withIndex()) {
            if (i != firstInd)
                minSquaredDist[i] = position.squaredEuclideanDistanceTo(nextCentroid)
        }

        while (centroids.size < k) {
            var distSum = 0.0
            for (i in positions.indices) {
                if (!taken[i])
                    distSum += minSquaredDist[i]
            }

            // Get a random number to choose x with a probability proportional to D(x)^2
            val r = random.nextDouble() * distSum
            var nextInd = -1
            var sum = 0.0

            // Find the first point whose summed distance is higher than r
            for (i in positions.indices) {
                if (!taken[i]) {
                    sum += minSquaredDist[i]
                    if (sum >= r) {
                        nextInd = i
                        break
                    }
                }
            }

            // If no point found pick up the first available one
            if (nextInd < 0) {
                for (i in positions.indices) {
                    if (!taken[i]) {
                        nextInd = i
                        break
                    }
                }
            }

            // If no available points (all are taken return)
            if (nextInd < 0)
                break

            nextCentroid = positions[nextInd]
            centroids.add(nextCentroid)
            taken[nextInd] = true

            if (centroids.size < k) {
                for ((i, position) in positions.withIndex()) {
                    if (!taken[i]) {
                        val d = nextCentroid.squaredEuclideanDistanceTo(position)
                        if (d < minSquaredDist[i])
                            minSquaredDist[i] = d
                    }
                }
            }
        }

        return centroids
    }

    private fun findNearestCentroid(position: Position, centroids: List<Position>): Pair<Int, Position> {
        var minDist = Double.MAX_VALUE
        var closestCentroidIndex = 0
        var closestCentroid = positions.first()

        // Find the closest centroid
        for ((i, centroid) in centroids.withIndex()) {

            if (centroid === position)
                return Pair(i, centroid)

            val d = position.squaredEuclideanDistanceTo(centroid)

            if (d < minDist) {
                minDist = d
                closestCentroidIndex = i
                closestCentroid = centroid
            }
        }

        return Pair(
            closestCentroidIndex,
            closestCentroid
        )
    }

    private fun assignPositionToCentroids(
        centroids: List<Position>,
        assignments: Array<Int>,
        newClusters: HashMap<Position, ArrayList<Position>>
    ): Boolean {
        var newAssign = false

        newClusters.clear()
        for (centroid in centroids)
            newClusters[centroid] = ArrayList()

        for ((i, position) in positions.withIndex()) {
            val cluster = findNearestCentroid(position, centroids)
            if (i != assignments[i])
                newAssign = true

            assignments[i] = cluster.first
            newClusters[cluster.second]!!.add(position)
        }

        return newAssign
    }

    @Throws(ArithmeticException::class)
    private fun solveEmptyCluster(clusters: Map<Position, List<Position>>): PositionMove {
        var maxSize = Int.MIN_VALUE
        var selectedCluster: List<Position>? = null
        var selectedCentroid: Position? = null

        //Find the biggest cluster
        for (cluster in clusters.entries) {
            if (cluster.value.size > maxSize) {
                maxSize = cluster.value.size
                selectedCentroid = cluster.key
                selectedCluster = cluster.value
            }
        }

        if (selectedCentroid == null || selectedCluster == null || selectedCluster.size < 2)
            throw ArithmeticException("Cannot converge to a solution")

        //Find the farthest point
        var selected = selectedCluster.first()
        var selectedInd = 0
        var maxDist = Double.NEGATIVE_INFINITY

        for ((i, position) in selectedCluster.withIndex()) {
            if (position === selectedCentroid)
                continue

            val d = position.squaredEuclideanDistanceTo(selectedCentroid)
            if (d > maxDist) {
                maxDist = d
                selected = position
                selectedInd = i
            }
        }

        return PositionMove(
            selected,
            selectedCentroid,
            selectedInd
        )
    }

    private fun computeCentroidOf(cluster: List<Position>) = Position.getMeanPosition(cluster)

    private fun computeInertia(actualFit: Map<Position, List<Position>>): Double {
        var inertia = 0.0

        for (entry in actualFit.entries)

            for (position in entry.value)
                inertia += position.squaredEuclideanDistanceTo(entry.key)

        return inertia
    }

    private fun interpolateInertiaAt(x: Int, x1: Int, y1: Double, x2: Int, y2: Double) =
        x * (y1 - y2) / (x1 - x2) + y1 - x1 * (y1 - y2) / (x1 - x2)

    enum class ClusteringMethod {
        ELBOW,
        CONSTANT
    }

    private data class PositionMove(
        val newCentroid: Position,
        val takenFromCentroid: Position,
        val selectionIndexInCluster: Int
    )

    companion object {
        const val DEFAULT_MAX_ITERATION = 50
        const val DEFAULT_K = 20
        const val DEFAULT_MAX_K = 2 * DEFAULT_K
        const val DEFAULT_MIN_K = DEFAULT_K / 2
        val DEFAULT_CLUSTERING_METHOD = ClusteringMethod.ELBOW

        private val random = Random(System.currentTimeMillis())
    }
}