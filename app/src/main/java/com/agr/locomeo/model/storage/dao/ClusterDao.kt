package com.agr.locomeo.model.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.PositionClusterCrossRef

@Dao
interface ClusterDao {
    @Query("SELECT * FROM clusters")
    fun getClusters(): List<Cluster>

    @Transaction
    @Query("SELECT * FROM position_cluster_cross_ref ORDER BY time ASC")
    fun getClusteredPosition(): List<PositionClusterCrossRef>

    @Transaction
    fun updateClusters(clusters: List<Cluster>) {
        deleteAll()
        insertAllClusters(clusters)
    }

    @Insert
    fun insertAllClusters(clusters: List<Cluster>)

    @Insert
    fun insertAllPositionClusterCrossRef(values: List<PositionClusterCrossRef>)

    @Query("DELETE FROM clusters")
    fun deleteAll()
}