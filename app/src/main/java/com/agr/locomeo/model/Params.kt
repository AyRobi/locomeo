package com.agr.locomeo.model

class Params {
    companion object {
        private const val PACKAGE_NAME = "com.agr.locomeo.model.params"

        const val LOCATION_AVAILABILITY_BROADCAST = "${PACKAGE_NAME}.location_availability_broadcast"
        const val ACTION_BROADCAST_NEW_SETTINGS = "$PACKAGE_NAME.new_settings_broadcast"
        const val EXTRA_IS_LOCATION_ALLOWED = "${PACKAGE_NAME}.is_extra_location_allowed"

        const val DEFAULT_LOCATION_REQUEST_INTERVAL = 10000L
        const val DEFAULT_LOCATION_REQUEST_FASTEST_INTERVAL = 5000L
        const val DEFAULT_MAX_REGISTERING_ACCURACY = 20.0f
        const val DEFAULT_MAX_COMPUTE_ACCURACY = 15.0f
        const val DEFAULT_PREDICTION_DURATION = 24 * 60 * 60 * 1000L
    }
}