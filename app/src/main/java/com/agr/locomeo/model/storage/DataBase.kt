@file:Suppress("unused")

package com.agr.locomeo.model.storage

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.location.Location
import androidx.room.Room
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.Position
import com.agr.locomeo.model.entity.PositionClusterCrossRef
import com.agr.locomeo.model.storage.db.AppDatabase

class DataBase constructor(_appCtx: Context){
    private val db: AppDatabase = Room.databaseBuilder(
        _appCtx,
        AppDatabase::class.java, "loco-meo-database"
    ).build()

    fun getLastPosition() = db.positionDao().getLastPosition()

    fun getAll() = db.positionDao().getAll()

    fun getLastEntries(minTime: Long): List<Position> = db.positionDao().getLastEntries(minTime)

    fun getLastEntries(minTime: Long, maxAccuracy: Float) = db.positionDao().getLastEntries(minTime, maxAccuracy)

    fun getClusters() = db.clusterDao().getClusters()

    fun getClusteredPosition() = db.clusterDao().getClusteredPosition()

    fun updateClusters(labels: List<String>, clusterMap: Map<Position, List<Position>>): List<Cluster> {
        val clusters = ArrayList<Cluster>(clusterMap.size)
        val crossRef = ArrayList<PositionClusterCrossRef>()

        for ((i, entry) in clusterMap.entries.withIndex()) {
            val cluster = Cluster(
                labels[i],
                entry.key,
                entry.value.size
            )

            clusters.add(cluster)

            for (position in entry.value)
                crossRef.add(PositionClusterCrossRef(
                    position.time,
                    cluster.label
                ))
        }

        db.runInTransaction {
            db.clusterDao().apply {
                updateClusters(clusters)
                insertAllPositionClusterCrossRef(crossRef)
            }

        }

        return clusters
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun insert(location: Location) = db.positionDao().insert(Position.fromLocation(location))

    fun insertIfPossible(location: Location) {
        try {
            insert(location)
        } catch (ex: SQLiteConstraintException) {
            // Silent exception
        }
    }

    fun insertAll(vararg location: Location) {
        val converted = ArrayList<Position>()

        for (loc in location) {
            converted.add(Position.fromLocation(loc))
        }

        db.positionDao().insertAll(*converted.toTypedArray())
    }

    fun delete(location: Location) = db.positionDao().delete(Position.fromLocation(location))

    fun clear() = db.clearAllTables()
}