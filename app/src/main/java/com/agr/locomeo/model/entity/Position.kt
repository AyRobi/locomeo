package com.agr.locomeo.model.entity

import android.location.Location
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlin.math.*

@Entity(tableName = "positions")
data class Position (
    @PrimaryKey val time: Long,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "altitude") val altitude: Double,
    @ColumnInfo(name = "bearing") val bearing: Float,
    @ColumnInfo(name = "accuracy") val accuracy: Float
) {
    @Ignore private var _ecefPoint: Point3D? = null
    private val ecefPoint: Point3D
        get() {
            if (_ecefPoint == null)
                _ecefPoint = computeEcefPoint()

            return _ecefPoint ?: throw AssertionError("Set to null by another thread")
        }

    fun squaredEuclideanDistanceTo(position: Position) =
        ecefPoint.squaredEuclideanDistanceTo(position.ecefPoint)

    private fun computeEcefPoint(): Point3D {
        val lat = latitude * DEG_TO_RAD
        val long = longitude * DEG_TO_RAD
//        val e2 = 1.0 / WGS84_RF * (2.0 - 1.0 / WGS84_RF)
        val sinLat = sin(lat)
        val cosLat = cos(lat)
        val chi = EARTH_RADIUS / sqrt(1 - e2 * sinLat * sinLat)

        return Point3D(
            (chi + altitude) * cosLat * cos(long),
            (chi + altitude) * cosLat * sin(long),
            (chi * (1 - e2) + altitude) * sinLat
        )
    }

    companion object {
        private const val DEG_TO_RAD = PI / 180
        private const val RAD_TO_DEG = 180 / PI
//        private const val WGS84_RF = 298.257223563
        private const val EARTH_RADIUS = 6378137.0
        private const val e2 = 6.6943799901377997e-3
        private const val a1 = 4.2697672707157535e+4
        private const val a2 = 1.8230912546075455e+9
        private const val a3 = 1.4291722289812413e+2
        private const val a4 = 4.5577281365188637e+9
        private const val a5 = 4.2840589930055659e+4
        private const val a6 = 9.9330562000986220e-1

        fun fromLocation(location: Location): Position {
            return Position(
                location.time,
                location.latitude,
                location.longitude,
                location.altitude,
                location.bearing,
                location.accuracy
            )
        }

        private fun ecefToGeoPoint(p: Point3D): Point3D {
            val x = p.x
            val y = p.y
            val z = p.z
            val zp = abs(z)
            val w2 = x.pow(2) + y.pow(2)
            val w = sqrt(w2)
            val r2 = w2 + z.pow(2)
            val r = sqrt(r2)
            val s2 = z.pow(2) / r2
            val c2 = w2 / r2
            var u = a2 / r
            var v = a3 - a4 / r

            val s: Double
            var lat: Double
            val ss: Double
            val c: Double
            if (c2 > 0.3) {
                s = (zp / r) * (1.0 + c2 * (a1 + u + s2 * v) / r)
                lat = asin(s)
                ss = s.pow(2)
                c = sqrt(1.0 - ss)
            } else {
                c = (w / r) * (1.0 - s2 * (a5 - u - c2 * v) / r)
                lat = acos(c)
                ss = 1.0 - c.pow(2)
                s = sqrt(ss)
            }

            val g = 1.0 - e2 * ss
            val rg = EARTH_RADIUS / sqrt(g)
            val rf = a6 * rg
            u = w - rg * c
            v = zp - rf * s
            val f = c * u + s * v
            val m = c * v - s * u
            val p2 = m / (rf / g + f)

            lat += p2

            return Point3D(
                (if (z < 0.0) -lat else lat) * RAD_TO_DEG,
                atan2(y, x) * RAD_TO_DEG,
                f + m * p2 / 2.0
            )
        }

        fun getMeanPosition(positions: List<Position>): Position {
            val n = positions.size

            if (n == 0)
                throw IllegalArgumentException("List is empty")

            if (n == 1)
                return positions.first()

            var timeSum = 0L
            var xSum = 0.0
            var ySum = 0.0
            var zSum = 0.0
            var bearingSum = 0f
            var accuracySum = 0f

            for (position in positions) {
                timeSum += position.time
                position.ecefPoint.let {
                    xSum += it.x
                    ySum += it.y
                    zSum += it.z
                }
                bearingSum += position.bearing
                accuracySum += position.accuracy
            }

            val geo = ecefToGeoPoint(
                Point3D(
                    xSum / n,
                    ySum / n,
                    zSum / n
                )
            )

            return Position(
                timeSum / n,
                geo.x,
                geo.y,
                geo.z,
                bearingSum / n,
                accuracySum / n
            )
        }
    }
}