package com.agr.locomeo.model

interface Updatable {
    fun onSettingsUpdate()
}