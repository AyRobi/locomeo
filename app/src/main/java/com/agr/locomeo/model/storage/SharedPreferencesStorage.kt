package com.agr.locomeo.model.storage

import android.content.SharedPreferences
import com.agr.locomeo.model.KMeansPP
import com.agr.locomeo.model.Params.Companion.DEFAULT_LOCATION_REQUEST_FASTEST_INTERVAL
import com.agr.locomeo.model.Params.Companion.DEFAULT_LOCATION_REQUEST_INTERVAL
import com.agr.locomeo.model.Params.Companion.DEFAULT_MAX_COMPUTE_ACCURACY
import com.agr.locomeo.model.Params.Companion.DEFAULT_MAX_REGISTERING_ACCURACY
import com.agr.locomeo.model.Params.Companion.DEFAULT_PREDICTION_DURATION

class SharedPreferencesStorage constructor(_pref: SharedPreferences) : Storage {
    private val pref = _pref

    override fun getServerIp() = pref.getString(SERVER_IP, null)

    override fun setServerIp(ip: String?) {
        pref.edit()
            .putString(SERVER_IP, ip)
            .apply()
    }

    override fun getLastClusteringComputationTime()
            = pref.getLong(LAST_CLUSTERING_COMPUTATION_TIME, 0)

    override fun setLastClusteringComputationTime(time: Long) {
        pref.edit()
            .putLong(LAST_CLUSTERING_COMPUTATION_TIME, time)
            .apply()
    }

    override fun getMaxRegisteringAccuracy() = pref.getFloat(
        MAX_REGISTERING_ACCURACY,
        DEFAULT_MAX_REGISTERING_ACCURACY
    )

    override fun setMaxRegisteringAccuracy(value: Float) {
        if (value <= 0f)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putFloat(MAX_REGISTERING_ACCURACY, value)
            .apply()
    }

    override fun getMaxComputeAccuracy() = pref.getFloat(
        MAX_COMPUTE_ACCURACY,
        DEFAULT_MAX_COMPUTE_ACCURACY
    )

    override fun setMaxComputeAccuracy(value: Float) {
        if (value <= 0f)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putFloat(MAX_COMPUTE_ACCURACY, value)
            .apply()
    }

    override fun getLocationRequestInterval() = pref.getLong(
        LOCATION_REQUEST_INTERVAL,
        DEFAULT_LOCATION_REQUEST_INTERVAL
    )

    override fun setLocationRequestInterval(value: Long) {
        if (value <= 0L)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putLong(LOCATION_REQUEST_INTERVAL, value)
            .apply()
    }

    override fun getLocationRequestFastestInterval() = pref.getLong(
        LOCATION_REQUEST_FASTEST_INTERVAL,
        DEFAULT_LOCATION_REQUEST_FASTEST_INTERVAL
    )

    override fun setLocationRequestFastestInterval(value: Long) {
        if (value <= 0L)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putLong(LOCATION_REQUEST_FASTEST_INTERVAL, value)
            .apply()
    }

    override fun getClusteringMethod(): KMeansPP.ClusteringMethod {
        return try {
            KMeansPP.ClusteringMethod.valueOf(
                pref.getString(CLUSTERING_METHOD, null) ?: KMeansPP.DEFAULT_CLUSTERING_METHOD.name
            )
        } catch (ex: IllegalArgumentException) {
            KMeansPP.DEFAULT_CLUSTERING_METHOD
        }
    }

    override fun setClusteringMethod(value: KMeansPP.ClusteringMethod) {
        pref.edit()
            .putString(CLUSTERING_METHOD, value.name)
            .apply()
    }

    override fun getClusterNumber() = pref.getInt(
        CLUSTER_NUMBER,
        KMeansPP.DEFAULT_K
    )

    override fun setClusterNumber(value: Int) {
        if (value <= 0)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putInt(CLUSTER_NUMBER, value)
            .apply()
    }

    override fun getPredictionDuration() = pref.getLong(
        PREDICTION_DURATION,
        DEFAULT_PREDICTION_DURATION
    )

    override fun setPredictionDuration(value: Long) {
        if (value <= 0)
            throw IllegalArgumentException("Value must be > 0")

        pref.edit()
            .putLong(PREDICTION_DURATION, value)
            .apply()
    }

    override fun isRequestingLocation() = pref.getBoolean(REQUEST_LOCATION, false)

    override fun setRequestingLocation(isRequesting: Boolean) {
        pref.edit()
            .putBoolean(REQUEST_LOCATION, isRequesting)
            .apply()
    }

    companion object {
        private const val SERVER_IP = "server_ip"
        private const val LAST_CLUSTERING_COMPUTATION_TIME = "last_clustering_computation_time"
        private const val REQUEST_LOCATION = "request_location"
        private const val MAX_REGISTERING_ACCURACY = "max_registering_accuracy"
        private const val MAX_COMPUTE_ACCURACY = "max_compute_accuracy"
        private const val LOCATION_REQUEST_INTERVAL = "location_request_interval"
        private const val LOCATION_REQUEST_FASTEST_INTERVAL = "location_request_fastest_interval"
        private const val CLUSTERING_METHOD = "clustering_method"
        private const val CLUSTER_NUMBER = "cluster_number"
        private const val PREDICTION_DURATION = "prediction_duration"
    }
}