package com.agr.locomeo.model

import com.agr.locomeo.model.entity.PositionClusterCrossRef

class MarkovMatrix(
    mapping: Map<String, Map<String, Float>>
) {
    private val internalMatrix = mapping

    fun getRowLabels() = internalMatrix.keys

    fun getProbability(from: String, to: String) = internalMatrix[from]?.get(to) ?: 0f

    fun getToProbabilities(from: String) = internalMatrix[from]

    companion object {
        /**
         * Note: locations are supposed to be sorted in the visit order, so taggedPositions[1] is visited
         * just after taggedPositions[0] and so on
         */
        fun fromClusteredPositions(taggedPositions: List<PositionClusterCrossRef>): MarkovMatrix {
            if (taggedPositions.size < 2)
                throw IllegalArgumentException("At least 2 positions are needed to compute the matrix")

            val mapping = HashMap<String, HashMap<String, Int>>()
            val totals = HashMap<String, Int>()

            var from = taggedPositions.first().clusterId

            for (position in taggedPositions.listIterator(1)) {
                val to = position.clusterId

                var row = mapping[from]
                if (row == null) {
                    row = HashMap()
                    mapping[from] = row
                }

                val count = (row[to] ?: 0) + 1
                val total = (totals[from] ?: 0) + 1

                row[to] = count
                totals[from] = total

                from = to
            }

            val markovMap = HashMap<String, HashMap<String, Float>>()
            for (entry in mapping.entries) {
                val row = HashMap<String, Float>()
                val total = totals[entry.key] ?: 0

                if (total == 0)
                    continue

                for (rowEntry in entry.value)
                    row[rowEntry.key] = rowEntry.value.toFloat() / total

                markovMap[entry.key] = row
            }

            return MarkovMatrix(markovMap)
        }
    }
}