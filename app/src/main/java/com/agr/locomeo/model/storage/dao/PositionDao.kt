package com.agr.locomeo.model.storage.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.agr.locomeo.model.entity.Position

@Dao
interface PositionDao {
    @Query("SELECT * FROM positions ORDER BY time DESC")
    fun getAll(): List<Position>

    @Query("SELECT * FROM positions WHERE time >= :minTime ORDER BY time DESC")
    fun getLastEntries(minTime: Long): List<Position>

    @Query("SELECT * FROM positions " +
            "WHERE time >= :minTime AND accuracy <= :maxAccuracy " +
            "ORDER BY time DESC")
    fun getLastEntries(minTime: Long, maxAccuracy: Float): List<Position>

    @Query("SELECT * FROM positions ORDER BY time DESC LIMIT 1")
    fun getLastPosition(): Position?

    @Insert
    fun insert(position: Position)

    @Insert
    fun insertAll(vararg position: Position)

    @Delete
    fun delete(position: Position)
}