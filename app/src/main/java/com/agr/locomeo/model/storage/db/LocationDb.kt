package com.agr.locomeo.model.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.agr.locomeo.model.entity.Cluster
import com.agr.locomeo.model.entity.Position
import com.agr.locomeo.model.entity.PositionClusterCrossRef
import com.agr.locomeo.model.storage.dao.ClusterDao
import com.agr.locomeo.model.storage.dao.PositionDao

@Database(entities = [
    Position::class,
    Cluster::class,
    PositionClusterCrossRef::class
], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun positionDao(): PositionDao
    abstract fun clusterDao(): ClusterDao
}