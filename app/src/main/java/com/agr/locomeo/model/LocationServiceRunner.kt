package com.agr.locomeo.model

interface LocationServiceRunner {
    fun startLocationService()
}