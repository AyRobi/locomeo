package com.agr.locomeo.model.storage

import com.agr.locomeo.model.KMeansPP

interface Storage {
    fun getServerIp() : String?
    fun setServerIp(ip: String?)

    fun getLastClusteringComputationTime(): Long
    fun setLastClusteringComputationTime(time: Long)

    fun getMaxRegisteringAccuracy(): Float
    fun setMaxRegisteringAccuracy(value: Float)

    fun getMaxComputeAccuracy(): Float
    fun setMaxComputeAccuracy(value: Float)

    fun getLocationRequestInterval(): Long
    fun setLocationRequestInterval(value: Long)

    fun getLocationRequestFastestInterval(): Long
    fun setLocationRequestFastestInterval(value: Long)

    fun getClusteringMethod(): KMeansPP.ClusteringMethod
    fun setClusteringMethod(value: KMeansPP.ClusteringMethod)

    fun getClusterNumber(): Int
    fun setClusterNumber(value: Int)

    fun getPredictionDuration(): Long
    fun setPredictionDuration(value: Long)

    fun isRequestingLocation() : Boolean
    fun setRequestingLocation(isRequesting: Boolean)
}