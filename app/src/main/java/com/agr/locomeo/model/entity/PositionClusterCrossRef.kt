package com.agr.locomeo.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "position_cluster_cross_ref",
    foreignKeys = [
        ForeignKey(
            entity = Position::class,
            parentColumns = arrayOf("time"),
            childColumns = arrayOf("time"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Cluster::class,
            parentColumns = arrayOf("cluster_id"),
            childColumns = arrayOf("cluster_id"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class PositionClusterCrossRef(
    @ColumnInfo(name = "time") @PrimaryKey val positionId: Long,
    @ColumnInfo(name = "cluster_id") val clusterId: String
)