package com.agr.locomeo.router

import android.content.Context
import android.content.Intent
import com.agr.locomeo.ui.activity.MainActivity

class MainRouter {
    companion object {
        fun openMain(ctx : Context) {
            val intent = Intent(ctx, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            ctx.startActivity(intent)
        }
    }
}