package com.agr.locomeo.util

import android.text.InputFilter
import android.text.Spanned

class PositiveIntFilter(
    private val min: Int = 1
): InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence {
        try {
            val number = toPositiveInt(dest.toString() + source.toString())
            if (number >= min)
                return source
        } catch (e : NumberFormatException) {
            // Do nothing
        }

        return ""
    }

    companion object{
        fun toPositiveInt(source: String): Int {
            var str = source

            if (str.endsWith('.') || str.endsWith(','))
                str += '0'

            val n = str.toInt()
            if (n < 0)
                throw NumberFormatException("Number must be positive")

            return n
        }

        fun toPositiveIntOrNull(source: String): Int? {
            return try {
                toPositiveInt(source)
            } catch (e: Exception) {
                null
            }
        }
    }
}