package com.agr.locomeo.util

import android.text.InputFilter
import android.text.Spanned

class PositiveFloatFilter(
    private val min: Float = 1.0f,
    private val precision: Int = 3
): InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence {
        try {
            var str = dest.subSequence(0, dstart).toString() +
                    source.subSequence(start, end).toString()

            if (dend != dest.length)
                str += dest.subSequence(dend, dest.length)

            val number = toPositiveFloat(str)

            if (str.length - str.indexOfAny(charArrayOf(',', '.')) - 1 > precision)
                return ""

            if (number >= min)
                return source
        } catch (e : NumberFormatException) {
            // Do nothing
        }

        return ""
    }

    companion object{
        fun toPositiveFloat(source: String): Float {
            var str = source

            if (str.endsWith('.') || str.endsWith(','))
                str += '0'

            val f = str.toFloat()
            if (f < 0f)
                throw NumberFormatException("Number must be positive")

            return f
        }

        fun toPositiveFloatOrNull(source: String): Float? {
            return try {
                toPositiveFloat(source)
            } catch (e : Exception) {
                null
            }
        }
    }
}