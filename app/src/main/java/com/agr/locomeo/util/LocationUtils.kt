package com.agr.locomeo.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager

class LocationUtils {
    companion object {
        fun isPermissionGranted(context: Context?) : Boolean {
            val locatePermissionState = context
                ?.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)

            return locatePermissionState == PackageManager.PERMISSION_GRANTED
        }
    }
}