package com.agr.locomeo.di

import com.agr.locomeo.ui.fragment.DebugFragment
import com.agr.locomeo.ui.fragment.MapFragment
import com.agr.locomeo.ui.fragment.PredictionFragment
import com.agr.locomeo.ui.fragment.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun bindDebugFragment() : DebugFragment

    @ContributesAndroidInjector
    abstract fun bindPredictionsFragment() : PredictionFragment

    @ContributesAndroidInjector
    abstract fun bindMapFragment() : MapFragment

    @ContributesAndroidInjector
    abstract fun bindSettingsFragment() : SettingsFragment
}