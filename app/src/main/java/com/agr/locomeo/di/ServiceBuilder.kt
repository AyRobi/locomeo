package com.agr.locomeo.di

import com.agr.locomeo.service.LocationService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ServiceBuilder {
    @ContributesAndroidInjector
    abstract fun bindLocationService(): LocationService
}