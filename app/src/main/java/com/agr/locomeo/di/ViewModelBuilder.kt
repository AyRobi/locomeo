package com.agr.locomeo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.agr.locomeo.viewmodel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelBuilder {

    @Binds
    internal abstract fun binViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun splashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DebugViewModel::class)
    internal abstract fun debugViewModel(viewModel: DebugViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PredictionsViewModel::class)
    internal abstract fun friendsViewModel(viewModel: PredictionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    internal abstract fun mapViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun settingsViewModel(viewModel: SettingsViewModel): ViewModel

}