package com.agr.locomeo.di

import android.content.Context
import com.agr.locomeo.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [
    ActivityBuilder::class,
    FragmentBuilder::class,
    ServiceBuilder::class,
    ViewModelBuilder::class,
    RepositoryModule::class,
    NetworkModule::class
])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App) : Context {
        return app.applicationContext
    }
}