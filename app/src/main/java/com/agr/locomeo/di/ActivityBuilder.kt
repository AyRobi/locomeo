package com.agr.locomeo.di

import com.agr.locomeo.ui.activity.MainActivity
import com.agr.locomeo.ui.activity.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindingSplashActivity() : SplashActivity

    @ContributesAndroidInjector
    abstract fun bindingMainActivity() : MainActivity
}