package com.agr.locomeo.di

import android.content.Context
import android.content.SharedPreferences
import com.agr.locomeo.model.storage.DataBase
import com.agr.locomeo.model.storage.SharedPreferencesStorage
import com.agr.locomeo.model.storage.Storage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Suppress("unused")
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideSharedPreference(ctx: Context) : SharedPreferences {
        return ctx.getSharedPreferences("prefs", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideStorage(pref: SharedPreferences) : Storage {
        return SharedPreferencesStorage(pref)
    }

    @Singleton
    @Provides
    fun provideDatabase(appCtx: Context): DataBase {
        return DataBase(appCtx)
    }
}