package com.agr.locomeo.service

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.agr.locomeo.R
import com.agr.locomeo.model.Params
import com.agr.locomeo.model.Params.Companion.ACTION_BROADCAST_NEW_SETTINGS
import com.agr.locomeo.model.Params.Companion.EXTRA_IS_LOCATION_ALLOWED
import com.agr.locomeo.model.Params.Companion.LOCATION_AVAILABILITY_BROADCAST
import com.agr.locomeo.model.Updatable
import com.agr.locomeo.model.storage.DataBase
import com.agr.locomeo.model.storage.Storage
import com.agr.locomeo.ui.activity.MainActivity
import com.google.android.gms.location.*
import dagger.android.DaggerService
import javax.inject.Inject
import kotlin.concurrent.thread

class LocationService: DaggerService(), Updatable {

    @Inject
    lateinit var storage: Storage

    @Inject
    lateinit var db: DataBase

    private val binder = LocalBinder()

    private var changingConfiguration = false

    private lateinit var locationRequest: LocationRequest
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var serviceHandler: Handler
    private lateinit var notificationManager: NotificationManager
    private lateinit var receiver: MyReceiver
    private var maxAccuracy: Float = Params.DEFAULT_MAX_COMPUTE_ACCURACY

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Location service created")

        receiver = MyReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            receiver,
            IntentFilter(ACTION_BROADCAST_NEW_SETTINGS)
        )

        maxAccuracy = storage.getMaxRegisteringAccuracy()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
                Log.d(TAG, "New availability " + locationAvailability?.toString())
                setRequesting(locationAvailability?.isLocationAvailable ?: false)
            }

            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult?.let { onNewLocation(it.lastLocation) }
            }
        }

        locationRequest = getLocationRequest(storage)
        getLastLocation()

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        serviceHandler = Handler(handlerThread.looper)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val appName = getString(R.string.app_name)
            val channel = NotificationChannel(
                CHANNEL_ID,
                appName,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "Location service started")

        val startedFromNotification = intent.getBooleanExtra(
            EXTRA_STARTED_FROM_NOTIFICATION, false
        )

        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        } else {
            addLocationUpdates()
        }

        return Service.START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        changingConfiguration = true
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "Location service bound")

        stopForeground(true)
        changingConfiguration = false

        return binder
    }

    override fun onRebind(intent: Intent) {
        Log.d(TAG, "Location service rebound")

        stopForeground(true)
        changingConfiguration = false

        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        Log.d(TAG, "Location service unbound")

        if (!changingConfiguration && storage.isRequestingLocation())
            startForeground(NOTIFICATION_ID, getNotification())

        return true
    }

    override fun stopService(name: Intent?): Boolean {
        Log.d(TAG, "Location service stopped")

        removeLocationUpdates()
        return super.stopService(name)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Location service destroyed")

        serviceHandler.removeCallbacksAndMessages(null)
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(receiver)
    }

    fun requestLocationUpdates() {
        startService(Intent(applicationContext, LocationService::class.java))
    }

    private fun addLocationUpdates() {
        setRequesting(true)

        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            ).addOnFailureListener {
                Log.d(TAG, "Operation not allowed by user")
                setRequesting(false)
            }
        } catch (e: SecurityException) {
            Log.d(TAG, "Operation not allowed by user")
            setRequesting(false)
        }
    }

    private fun removeLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
            .addOnFailureListener {
                Log.d(TAG, "Operation not allowed by user")

                setRequesting(true)
            }

        setRequesting(false)
    }

    private fun getNotification(): Notification {
        val intent = Intent(this, LocationService::class.java)

        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

        val pendingIntentService = PendingIntent.getService(
            this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        val pendingIntentActivity = PendingIntent.getActivity(
            this,0, Intent(this, MainActivity::class.java), 0
        )

        val text = getString(R.string.notification_text)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .addAction(
                R.drawable.ic_launch,
                getString(R.string.notification_launch),
                pendingIntentActivity
            ).addAction(
                R.drawable.ic_cancel,
                getString(R.string.notification_cancel),
                pendingIntentService
            ).setContentText(
                text
            ).setContentTitle(
                getString(R.string.notification_title)
            ).setOngoing(true)
            .setNotificationSilent()
            .setSmallIcon(R.mipmap.ic_launcher)
            .setTicker(text)
            .setWhen(System.currentTimeMillis())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.priority = NotificationManager.IMPORTANCE_HIGH
        } else {
            @Suppress("DEPRECATION")
            builder.priority = Notification.PRIORITY_HIGH
        }

        return builder.build()
    }

    private fun getLastLocation() {
        try {
            fusedLocationClient.lastLocation
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        it.result?.let { location ->
                            storeLocation(location)
                        }
                    }
                }.addOnFailureListener {
                    Log.d(TAG, "Failed to retrieve last location")
                    setRequesting(false)
                }
        } catch (e: SecurityException) {
            Log.d(TAG, "Failed to retrieve last location")
            setRequesting(false)
        }
    }

    private fun setRequesting(isRequesting: Boolean) {
        sendIsAllowedBroadcast(isRequesting)
        storage.setRequestingLocation(isRequesting)
    }

    private fun sendIsAllowedBroadcast(locationAllowed: Boolean = false) {
        Log.d(TAG, "Message broadcast")
        val intent = Intent(LOCATION_AVAILABILITY_BROADCAST)
        intent.putExtra(EXTRA_IS_LOCATION_ALLOWED, locationAllowed)
        LocalBroadcastManager.getInstance(applicationContext)
            .sendBroadcast(intent)
    }

    private fun onNewLocation(location: Location) {
        Log.d(TAG, "New location received$location")
        storeLocation(location)
    }

    private fun storeLocation(location: Location) {
        if (location.accuracy > maxAccuracy)
            return

        thread(start = true) {
            db.insertIfPossible(location)
        }
    }

    override fun onSettingsUpdate() {
        Log.d(TAG, "Request settings updated")
        maxAccuracy = storage.getMaxRegisteringAccuracy()
        locationRequest = getLocationRequest(storage)

        if (storage.isRequestingLocation()) {
            removeLocationUpdates()
            addLocationUpdates()
        }
    }

    inner class LocalBinder: Binder() {
        fun getService() : LocationService {
            return this@LocationService
        }
    }

    inner class MyReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            this@LocationService.onSettingsUpdate()
        }

    }

    companion object {
        private val TAG = LocationService::class.java.simpleName
        private const val PACKAGE_NAME = "com.agr.locomeo.service.locationservice"
        private const val CHANNEL_ID = "LocationChannel"
        private const val NOTIFICATION_ID = 0x42
        private const val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"

        fun getLocationRequest(storage: Storage): LocationRequest {
            return LocationRequest.create().apply {
                interval = storage.getLocationRequestInterval()
                fastestInterval = storage.getLocationRequestFastestInterval()
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
        }
    }
}